<div align="center">
  <a href="https://www.jcs-profile.com/" target="_blank">
    <img alt="Profile" src="https://www.jcs-profile.com/images/icons/profile.svg" width="5%"/>
  </a>
  &nbsp;•&nbsp;
  <a href="https://github.com/jcs090218" target="_blank">
    <img alt="GitHub" src="https://www.jcs-profile.com/images/icons/github.svg" width="5%"/>
  </a>
  &nbsp;•&nbsp;
  <a href="https://twitter.com/jenchieh94" target="_blank">
    <img alt="Twitter" src="https://www.jcs-profile.com/images/icons/twitter.svg" width="5%"/>
  </a>
  &nbsp;•&nbsp;
  <a href="https://www.linkedin.com/in/jen-chieh-shen-17a02780/" target="_blank">
    <img alt="LinkedIn" src="https://www.jcs-profile.com/images/icons/linkedin.svg" width="5%"/>
  </a>
  &nbsp;•&nbsp;
  <a href="mailto:jcs090218@gmail.com" target="_blank">
    <img alt="Mail" src="https://www.jcs-profile.com/images/icons/gmail.svg" width="5%"/>
  </a>
  &nbsp;•&nbsp;
  <a href="https://jcs090218.github.io/blog/" target="_blank">
    <img alt="Blog" src="https://www.jcs-profile.com/images/icons/blog.svg" width="5%"/>
  </a>
</div>

<div align="center">Information above...</div>
